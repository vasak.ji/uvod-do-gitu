Purkmistr Přemysl z Nezamyslic vymyslel jednou mimoděk takovou nesmyslnou věc. 
Aby prý se vymýtil konečně všechen hmyz a myšky, které se neustále ochomýtaly kolem domácího sýra, bylo podle něj nutné rozmístit do všech koutů místnosti dva dny staré pomyje. 
Ten nápad se vymykal veškerému chápání, ale přesto ho museli sluhové zrealizovat. 
Po dvou dnech se z domu linul mírný zápach. Nakonec pán musel uznat, že se zmýlil. 
Sluhové pak museli celý dům vysmýčit, a tím se zbavili veškerých nežádoucích zvířecích příživníků. 
